# parallel-homework

This is a submission for the engineering challenge at https://gist.github.com/bmuller/341e89cf87083119ad1241f5b896fa7c.

Steps to run:

- Please setup a Postgres instance (build against v11) serving on `localhost:25060`.
- Run `./db-init.sql` to create the `parallel` database, schema, roles, and grants.
  - Note: I did not test line 1, creating of the database.
- Run the sever (`go run main.go`).
  - Note: Requires port `8080` to be available.
- Open browser to `http://localhost:8080/`

When the form is submitted: an investor record will be added to the database in the `investors` table; uploaded documents will be added to the `./docs` directory and a record will be added to the `investor_documents` table. Note: investors with the same name, and documents with the same name for an investor will be overwritten.


Given more time (and a real product), I'd seek to include:

- Better documentation.
- Support for configuration from environment and flags.
- Decompose the code into multiple packages for different domains and abstraction layers.
- Unit tests and mock implementations of dependencies.
- Integration tests.
- Deployment artifacts (systemd unit file, docker config, etc.).
- Further understanding of the domain(s) and handling of more use cases.
- Support for CORS.
- Endpoint handlers would be wrapped with tracing middleware.
- Errors would log and return a request ID for reference.
- Given that this domain deals with PII, I would look to decompose the data records into discrete PII chunks (names, dob, phone numbers, addresses, etc) and store each under difference primary keys that are derived by a one-way cryptographic hash based on the account id and subdomain key.
- Support for token based authentication on protected endpoints with varying permissions granted per token. My preferred token setup is based on JWT using ed25519 signing keys.
  - User signs in and is issued an access token (stored in sessionStore) and a refresh token (stored in localStore). Assess token is included in the `Authorization` header of every request. The handler retrieves the public key identified in the token and verifies the signature and claims.
- CSS styling for the form.
- JavaScript validation of the form fields.
- UX for the form submission result, both success and errors. Preferably submitting the form without a page load.
- Setup CI.