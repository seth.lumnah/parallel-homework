CREATE DATABASE parallel;

CREATE TABLE investors (
    id bytea PRIMARY KEY,
    first_name text,
    last_name text,
    dob date,
    phone_number text,
    street_address text,
    state text,
    zip_code text
);

CREATE UNIQUE INDEX investors_pkey ON investors (id bytea_ops);

CREATE TABLE investor_documents (
    id bytea,
    filename text
);

CREATE INDEX investor_documents_id_filename_idx ON investor_documents (id bytea_ops, filename text_ops);

CREATE ROLE parallel WITH LOGIN ENCRYPTED PASSWORD 'abc123' NOSUPERUSER NOCREATEDB NOCREATEROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON investors TO "parallel";

GRANT DELETE, INSERT, SELECT, UPDATE ON investor_documents TO "parallel";

