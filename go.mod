module gitlab.com/seth.lumnah/parallel-homework

go 1.14

require (
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/jackc/pgx/v4 v4.7.0
)
