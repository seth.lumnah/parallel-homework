package main

import (
	"context"
	"crypto/sha512"
	"database/sql"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"

	_ "github.com/jackc/pgx/v4/stdlib"
)

type Investor struct {
	FirstName     string
	LastName      string
	DateOfBirth   time.Time
	PhoneNumber   string
	StreetAddress string
	State         string
	ZipCode       string
}

// ID returns SHA512 hash of inv.LastName + "\" + inv.FirstName.
func (inv Investor) ID() []byte {
	h := sha512.New()
	fmt.Fprintf(h, "%s\\%s", inv.LastName, inv.FirstName)
	return h.Sum(nil)
}

type Store interface {
	Save(context.Context, Investor) error
	SaveDocument(context.Context, Investor, string, io.Reader) error
}

var _ Store = InvestorStore{}

type InvestorStore struct {
	DB  *sql.DB
	Dir string // file system root directory
}

func (s InvestorStore) Save(ctx context.Context, inv Investor) error {
	const query = "INSERT INTO investors (id, first_name, last_name, dob, phone_number, street_address, state, zip_code) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) ON CONFLICT (id) DO UPDATE SET dob = $4, phone_number = $5, street_address = $6, state = $7, zip_code = $8"

	_, err := s.DB.ExecContext(ctx, query, inv.ID(), inv.FirstName, inv.LastName, inv.DateOfBirth, inv.PhoneNumber, inv.StreetAddress, inv.State, inv.ZipCode)
	if err != nil {
		return err
	}

	return nil
}

func (s InvestorStore) SaveDocument(ctx context.Context, inv Investor, filename string, r io.Reader) error {
	const query = "INSERT INTO investor_documents (id, filename) VALUES ($1, $2)"

	// hashing the filename
	h := sha512.New()
	h.Write(inv.ID())
	fmt.Fprintf(h, "%s", filename)
	name := h.Sum(nil)

	// ensure the directory exists
	path := fmt.Sprintf("%s/%x/%x", s.Dir, name[0], name[1])
	if err := os.MkdirAll(path, 0770); err != nil {
		return err
	}

	// save file
	file := fmt.Sprintf("%s/%x", path, name)
	f, err := os.Create(file)
	if err != nil {
		return err
	}
	defer f.Close()

	// update db
	if _, err := s.DB.ExecContext(ctx, query, inv.ID(), filename); err != nil {
		return err
	}

	return nil
}

type Server struct {
	Store   Store
	Logger  *log.Logger
	handler http.Handler
	once    sync.Once
}

func (s *Server) handleGetForm() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./form.html")
	}
}

func (s *Server) handlePostForm() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := r.ParseMultipartForm(1024 * 1024); err != nil {
			s.Logger.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		dob, err := time.Parse("01/02/2006", r.FormValue("dob"))
		if err != nil {
			s.Logger.Println(err)
			fmt.Fprint(w, "dob must be in MM/DD/YYYY format")
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		inv := Investor{
			FirstName:     r.FormValue("first_name"),
			LastName:      r.FormValue("last_name"),
			DateOfBirth:   dob,
			PhoneNumber:   r.FormValue("phone_number"),
			StreetAddress: r.FormValue("street_address"),
			State:         r.FormValue("state"),
			ZipCode:       r.FormValue("zip_code"),
		}

		if err := s.Store.Save(r.Context(), inv); err != nil {
			s.Logger.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		for _, v := range r.MultipartForm.File {
			for _, fh := range v {
				f, err := fh.Open()
				if err != nil {
					s.Logger.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
				defer f.Close()

				if err := s.Store.SaveDocument(r.Context(), inv, fh.Filename, f); err != nil {
					s.Logger.Println(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
			}
		}

		w.WriteHeader(http.StatusOK)
	}
}

func (s *Server) initHandlers() {
	r := mux.NewRouter()
	s.handler = r

	r.Handle("/", handlers.MethodHandler{
		http.MethodGet:  s.handleGetForm(),
		http.MethodPost: s.handlePostForm(),
	})
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.once.Do(s.initHandlers)
	s.handler.ServeHTTP(w, r)
}

func run(ctx context.Context, stderr io.Writer) error {
	logger := log.New(stderr, "", log.LstdFlags)

	db, err := sql.Open("pgx", os.Getenv("DATABASE_URL"))
	if err != nil {
		return err
	}
	defer db.Close()

	svr := &http.Server{
		Addr: ":8080",
		Handler: &Server{
			Store: InvestorStore{
				DB:  db,
				Dir: "./docs",
			},
			Logger: logger,
		},
		ErrorLog: logger,
	}

	go func() {
		if err := svr.ListenAndServe(); err != nil {
			logger.Println(err)
		}
	}()

	<-ctx.Done()
	return svr.Shutdown(context.Background())
}

func contextWithSignal(ctx context.Context, sig ...os.Signal) (context.Context, context.CancelFunc) {
	ctx, cancel := context.WithCancel(ctx)

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, sig...)

	go func() {
		select {
		case <-ctx.Done():
		case <-ch:
			cancel()
		}

		signal.Stop(ch)
		close(ch)
	}()

	return ctx, cancel
}

func main() {
	ctx, cancel := contextWithSignal(context.Background(), os.Interrupt)
	defer cancel()

	os.Setenv("DATABASE_URL", "postgresql://parallel:abc123@localhost:25060/parallel?sslmode=disable")

	if err := run(ctx, os.Stderr); err != nil {
		log.Println(err)
	}
}
